$(document).ready(function() {

  var loader = '<div class="loader_container"><i class="loader icon-action-settings"></i></div>';

  // Load the compnent HTML
  $(".card .anchor").each(function () {

    var componentId = $(this).attr("id");
    var componenthtml = "html/" + componentId +".html";
    $(this).siblings(".accordion_panel").append(loader);

    $.ajax({
      url: componenthtml,
      beforeSend: function() { $('.loader_container').show(); },
      complete: function() { $('.loader_container').hide(); },
      success: function(componenthtml) {
        $("#" + componentId).siblings(".accordion_panel").find(".component_container").html(componenthtml);
      },
      async: false
    });

  });

  // Add the code container to the page.
  $(".accordion_panel .source-code").each(function () {
    $(this).append($(".code_container_template").html());
  });

  $(".icon-source-code").on("click", function() {

    // Show the source code.
    var _component_html = $(this).parents(".source-code").siblings(".component_container").html();
    $(this).siblings(".code_container").find(".code_html").text(_component_html);

    // Change icons.
    $(this).hide();
    $(this).siblings(".icon-copy").fadeIn(200);
    $(this).siblings('.icon-decline').fadeIn(200);
    $('.icon-decline').on('click', function () {
      $(this).siblings(".code_container").fadeOut();
      $(this).siblings(".icon-source-code").fadeIn();
      $(this).hide();
    });

    // Toggle the code container.
    $(this).siblings(".code_container").fadeIn(200);

    // Copy the source code.
    $(this).siblings(".icon-copy").on("click", function() {
      $(this).siblings(".code_container").find(".code_html").select();
      document.execCommand("copy");
    });
  });

  // // Add the JS code
  // var tab_bar_btn_js = '<button class="tab_bar_btn">JavaScript</button>'
  // var tab_content_code_js = '<textarea class="tab_content code_js" name="JavaScript" rows="20" cols="100"></textarea>'
  //
  // $("#componentButtonGroup").siblings(".accordion_panel").find(".source-code .tab").append(tab_bar_btn_js);
  // $("#componentButtonGroup").siblings(".accordion_panel").find(".source-code .tab_content_container").append(tab_content_code_js);

  // Component page side nav search
  $("#cl-sidenav_search").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#cl-sidenav_content *").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  // Component page page side navigation
  $(".cl-sidenav_dropdown_btn").on("click", function() {
    $(this).next().slideToggle("fast");
  });

  // Component page side navigation on tablet

  function sidenavClose() {
    $(".cl-page_overlay").hide();
    $(".cl-sidenav").animate({
      width: '0px'
    }, 200);
  };

  $("header .icon-menu").on("click", function () {
    $(".cl-sidenav").find(".cl-sidenav_item").addClass("cl-sidenav_item--tablet")
    $(".cl-page_overlay").show();
    $(".cl-sidenav").animate({
      width: '256px'
    }, 200);
  });

  $("body").on("click", ".cl-sidenav_item--tablet, .cl-page_overlay", function () {
    sidenavClose();
  })

  $(window).resize(function() {
    if ($(window).width() <= 768) {
      sidenavClose();
    } else {
      $(".cl-sidenav").find(".cl-sidenav_item").removeClass("cl-sidenav_item--tablet");
      $(".cl-page_overlay").hide();
      $(".cl-sidenav").animate({
        width: '256px'
      }, 200);
    }
  });

  // ====================   Component   ====================

  // side navigation
  $(".sidenav_item").on("click", function() {
    $(this).siblings(".sidenav_item").removeClass("sidenav_item--active");
    $(this).addClass("sidenav_item--active");
    $(".page_layout").hide();
    var pageID = $(this).attr("id");
    $("#" + pageID + "Page").show();
  });

  // Side navigation on tablet

  $(".page-header_menu").on("click", function () {
    $(".sidenav").animate({width:'toggle'},200);
    $(".page_layout").toggleClass("page_layout--tablet");
  });

  // Accordion
  $("body").on("click", ".accordion_header", function() {
    $(this).next(".accordion_panel").toggleClass(".accordion_panel--collapsed");
    $(this).toggleClass("accordion_header--collapsed");
  });

  //Button Group
  $("body").on("click", ".btn--group_btn", function () {
    $(this).siblings(".btn--group_btn").removeClass("btn--group_btn--active");
    $(this).addClass("btn--group_btn--active");
  });

  // tooltips
  $("body").on("mouseenter", ".btn--lg--icon, .btn--sm--icon, .label--status--dot", function() {
    var _btn_title = $(this).attr("title");
    $(this).append('<span class="tooltip">' + _btn_title + '</span>');
  });

  // Tabs
  $("body").on("click", ".tab_bar_btn", function() {
    $(this).siblings(".tab_bar_btn").removeClass("tab_bar_btn--active");
    $(this).addClass("tab_bar_btn--active");
    var tab = $(this).html();
    $(this).parents(".tab_container").find(".tab_content_container .tab_content").removeClass("tab_content--active");
    $(this).parents(".tab_container").find(".tab_content_container .tab_content[name = " + tab + "]").addClass("tab_content--active");
  });
  $(".tab_bar_btn--active").click();

  // Run-time tab
  $(".tab--run-time_title").each(function() {
    var _tab_title = $(this).text();
    $(this).prop("title", _tab_title);
  });

  $("body").on("click", ".tab--run-time_item", function() {
    $(this).siblings().removeClass("tab--run-time_item--active");
    $(this).addClass("tab--run-time_item--active");
  });

  //Number format
  const numberWithCommas = (x) => {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
  }

  $(function() {
    $(".tab--run-time_figure").each(function(_figure){
      var _number = $(this).html();
      _number = numberWithCommas(_number);
      $(this).html(_number);
    });
  });

  // topnav
  $("body").on("click", ".topnav_tab_item", function(){
    $(this).siblings().removeClass("topnav_tab_item--active");
    $(this).addClass("topnav_tab_item--active");
  });

  //back to top
  $(window).scroll(function() {
    if ($(this).scrollTop()) {
      $('#back-to-top').fadeIn();
    } else {
      $('#back-to-top').fadeOut();
    }
  });

  $("body").on("click", "#back-to-top", function () {
    $("html, body").animate({scrollTop: 0}, 1000);
  });

  //Autocomplete
  function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;

    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "_autocomplete_list");
      a.setAttribute("class", "autocomplete_list");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          $(b).addClass("autocomplete_list_item")
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
            /*insert the value for the autocomplete text field:*/
            inp.value = this.getElementsByTagName("input")[0].value;
            /*close the list of autocompleted values,
            (or any other open lists of autocompleted values:*/
            closeAllLists();
          });
          a.appendChild(b);
        }
      }
    });

    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
    });

    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      // x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      var x = document.getElementsByClassName("autocomplete_list");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
      closeAllLists(e.target);
    });
  };

  /*An array containing all the country names in the world:*/
  var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

  /*initiate the autocomplete function on the "search-contries" element, and pass along the countries array as possible autocomplete values:*/
  autocomplete(document.getElementById("search-contries"), countries);

  //Modal
  $("body").on("click", ".openModal", function() {
    var modalID = $(this).attr("id") + "Panel";
    $(this).siblings("#" + modalID + "").show();
    $(".modal_close, .modal_footer button[name='cancel']").click(function() {
      $(this).parents(".modal").hide();
    });
  });

  //Alert

  $("#openAlertDanger").on("click", function () {
    $(this).parents(".topnav").hide();
    $(this).parents(".topnav").siblings(".alert").fadeIn();
  });

  $("body").on("click", ".alert .icon-decline", function() {
    $(this).parents(".alert").hide();
    $(this).parents(".alert").siblings(".topnav").fadeIn();
  });

  //DropDown

  $("body").on("click", ".dropdown_btn", function() {
    $(this).siblings(".dropdown_list").slideToggle("fast");
    $(this).siblings(".icon-slim-arrow-down").toggleClass("icon-slim-arrow-up");
  });

  $("body").on("blur", ".dropdown_btn", function() {
    $(this).siblings(".dropdown_list").slideUp("fast");
    $(this).siblings(".icon-slim-arrow-down").removeClass("icon-slim-arrow-up");
  });

  $(".dropdown_list li").addClass("dropdown_list_item");

  $(".dropdown_list_item").click(function() {
    var text = $(this).text();
    $(this).closest("form").find("input").val(text);
  })

  $("body").on("keyup", ".dropdown input:text", function() {
    var value = $(this).val().toLowerCase();
    $(this).siblings(".dropdown_list").find("*").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  function filterFunction() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("search-index-dropdown-filter");
    filter = input.value.toUpperCase();
    div = document.getElementById("dropdown-with-filter-content");
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
      if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = "";
      } else {
        a[i].style.display = "none";
      }
    }
  }

  // toggle hide and show

  $(".toggle_show-more").siblings(".toggle_content").hide();

  $("body").on("click", ".toggle_show-more", function() {
    $(this).siblings(".toggle_content").slideToggle();
    $(this).removeClass("toggle_show-more");
    $(this).addClass("toggle_show-less");
  });

  $("body").on("click", ".toggle_show-less", function() {
    $(this).siblings(".toggle_content").slideToggle();
    $(this).removeClass("toggle_show-less");
    $(this).addClass("toggle_show-more");
  });

  $(".progress_bar").hide();
  $("button:contains('Save')").on("click", function() {
    var _progress_bar = $(this).parents(".topnav").siblings(".progress_bar_container").find(".progress_bar")
    $(_progress_bar).show();
    $(_progress_bar).animate({
      width: "100%",}, 1500
    );
    $(_progress_bar).fadeOut();
  });

  // combobox

  $("body").on("click", ".combobox_dropdown_btn", function() {
    $(this).siblings(".combobox_dropdown_list").slideDown("fast");
    $(this).addClass("combobox-active");
  });

  $(document).bind('click', function(e) {
    var $clicked = $(e.target);
    if (!$clicked.parents().hasClass("combobox")) {
      $(".combobox_dropdown_list").slideUp("fast");
      $(".combobox_dropdown_btn").removeClass("combobox-active");
    }
  });

  $('.combobox_dropdown_list input[type="checkbox"]').each(function(){
    var title = $(this).val();
    if ($(this).is(':checked')) {
      var html = '<span title="' + title + '">' + title + '</span>';
      $('.combobox_selected_list').append(html);
      $(".combobox_dropdown_btn_hint").hide();
    }
  });

  $(".combobox_selected_list span").addClass("label--selected");

  $("body").on("click", ".combobox_dropdown_btn span", function combobox_remove(){
    var combobox_val = $(this).text();
    $(this).parents(".combobox").find(":input[value=" + combobox_val + "]").prop("checked", false);
    $(this).fadeOut(200, function() {
      $(this).remove();
    });
  });

  $(function combobox(){

    $('.combobox_dropdown_list input[type="checkbox"]').on('click', function() {

      var title = $(this).closest('.combobox_dropdown_list').find('input[type="checkbox"]').val(),
      title = $(this).val() + "";

      if ($(this).is(':checked')) {
        var html = "<span title='" + title + "' class='label--selected'>" + title + '<i class="icon-decline"></i></span>';
        $(html).appendTo('.combobox_selected_list').hide().fadeIn(200);
        $(".combobox_dropdown_btn_hint").hide();
      } else {
        $('span[title="' + title + '"]').fadeOut(200, function() {
          $(this).remove();
        });
        var ret = $(".combobox_dropdown_btn_hint");
        $('.combobox_dropdown_btn').append(ret);
      }

      $('.combobox_dropdown_btn span').on('click', function combobox_remove(){
        var combobox_val = $(this).text();
        $(this).parents(".combobox").find(":input[value=" + combobox_val + "]").prop("checked", false);
        $(this).fadeOut(200, function() {
          $(this).remove();
        });
      });
    });
  });

  //labels
  $(".label--selected").append("<i class='icon-decline'></i>");
  $(".label--selected--context").prepend("Context: ");
  $(".label--selected").on("click", function() {
    $(this).fadeOut(200, function () {
      $(this).remove();
    });
  });

  // pagination
  $(".pagination a").addClass("pagination_item");
  $(".pagination_item").on("click", function() {
    $(this).siblings().removeClass("pagination_item--active");
    $(this).addClass("pagination_item--active");
  });

  // show toast
  $("body").on("click", "button[type=submit]", function () {
    $(this).siblings(".toast").fadeIn();
    $(this).siblings(".toast").delay(1800).fadeOut();
  });


  // Responsive table
  $(".table_container--list").each(function() {
    tableHeader = $(this).find("thead tr th");
    for (i = 0; i < tableHeader.length; i++) {
      var dataLabel = $(this).find("thead tr th").eq(i).text();
      $(this).find("tbody tr").each(function() {
        $(this).find("th, td").eq(i).attr("data-label", "" + dataLabel + "");
      });
    }
  });

});

//block button

function appendBlock() {
  var newBlock = `
  <div class="card_content_body fade">Please avoid putting the block button on the grey background <code>class="card_content_body"</code> as they should be on the same level.</div>
  `;
  $(".appendBlock").append(newBlock);
};

function prependBlock() {
  var newBlock = `
  <div class="card_content_body fade">Please avoid putting the block button on the grey background <code>class="card_content_body"</code> as they should be on the same level.</div>
  `;
  $(".prependBlock").prepend(newBlock);
};
