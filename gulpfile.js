'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
/**
 *  Default task clean temporaries directories and launch the
 *  main optimization build task
 */
gulp.task('default', function () {
  return gulp.src('scss/discovery-components.scss') // Gets all files ending with .scss in app/scss and children dirs
    .pipe(sass())
    .pipe(gulp.dest('dist/'))
});
